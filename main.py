
import pymysql
from ib_insync import *
from config import configs

ib = IB()
ib.connect(configs['ib']['host'], configs['ib']['port'], clientId=configs['ib']['client_id'])

connect = pymysql.connect(
        host=configs['db']['host'],
        db=configs['db']['database'],
        user=configs['db']['user'],
        passwd=configs['db']['password'],
        charset='utf8',
        use_unicode=True
)

def save_stock_data(code, durationStr="2 Y", first_insert=True):
        df = None
        try:
                contract = Stock(code, exchange="SMART", currency="USD")

                bars = ib.reqHistoricalData(contract,
                                            endDateTime='', durationStr=durationStr,
                                            barSizeSetting='1 day', whatToShow='MIDPOINT', useRTH=True)

                # convert to pandas dataframe:
                df = util.df(bars)
                print(df[['date', 'open', 'high', 'low', 'close', 'volume']])
        except Exception as e:
                print(str(e))

        if df is not None:
                insert_arg = []
                for i in range(len(df)):
                        insert_arg.append((df['date'][i], code, float(df['open'][i]), float(df['high'][i]),
                                float(df['low'][i]),
                                float(df['close'][i])))

                insert_sql = "insert into `stock_day`(`date`, `code`, `open`, `high`, `low`, `close`) values(%s, %s, %s, %s, %s, %s)"
                cursor = connect.cursor()

                if first_insert:
                        try:
                                cursor.executemany(insert_sql, insert_arg)
                                connect.commit()
                        except Exception as e:
                                print(str(e))
                else:
                        for arg in insert_arg:
                                try:
                                        cursor.execute(insert_sql, arg)
                                        connect.commit()
                                except Exception as e:
                                        print(str(e))



if __name__=="__main__":
        save_stock_data("qqq", "2 Y", False)