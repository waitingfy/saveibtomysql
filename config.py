configs = {
    'debug': False,
    'db': {
        'host': '127.0.0.1',
        'port': 3306,
        'user': 'root',
        'password': '',
        'database': 'trade_db'
    },
    'ib': {
        'host': '127.0.0.1',
        'port': 7497,
        'client_id': 2
    }
}
